class Database:
    def __init__(self):
        self.data = dict()

    def add(self, id: str, document):
        self.data[id] = document

    def update(self, id: str, document):
        return self.data.update({id: document})

    def get(self, id: str):
        return self.data[id]

    def remove(self, id: str):
        return self.data.pop(id)