import re
from database import Database

class PositionnalIndex:
    def __init__(self, database: Database, index: dict = {}):
        self.database = database
        self.index = index

    def __repr__(self):
        return str(self.index)

    def index_document(self, id: str, document: str):
        clean_text = re.sub(r'[^\w\s]', '', document)
        tokens = clean_text.split(' ')
        for i in range(len(tokens)):
            token = tokens[i]
            if token not in self.index.keys():
                self.index[token] = {}
            if id not in self.index[token].keys():
                self.index[token][id] = []
            self.index[token][id].append(i)
        self.database.add(id, document)
        return document

    def index_documents(self, documents: list):
        for document in documents:
            id = documents.index(document)
            self.index_document(id, document)

    def lookup(self, terms: list):
        """Search fo multiple terms

        Args:
            terms (list): looking for these terms in all indexed documents

        Return:
            docs (list): list of documents' ID containing all terms
        """
        docs = []
        for term in terms:
            if not term in self.index.keys():
                continue
            if docs == []:
                docs = list(self.index[term].keys())
            else:
                term_related_docs = list(self.index[term].keys())
                for doc in docs:
                    if not doc in term_related_docs:
                        docs.remove(doc)
        return docs
