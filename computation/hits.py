import networkx as nx

G = nx.DiGraph()
G.add_edges_from([
    ('A', 'D'), ('B', 'C'), ('B', 'E'), ('C', 'A'),
    ('D', 'C'), ('E', 'D'), ('E', 'B'), ('E', 'F'),
    ('E', 'C'), ('F', 'C'), ('F', 'H'), ('G', 'A'),
    ('G', 'C'), ('H', 'A')])

hubs, authorities = nx.hits(G, max_iter = 50, normalized = True)
# The in-built hits function returns two dictionaries keyed by nodes
# containing hub scores and authority scores respectively.

print("# Hubs")
for key in hubs:
    print("{key} -> {value}".format(key=key, value=hubs[key]))

print("# Authorities")
for key in authorities:
    print("{key} -> {value}".format(key=key, value=authorities[key]))