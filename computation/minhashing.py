import numpy as np

def hash(x):
    return (x+1)%5

def minhash(data: list, hash):
    res = np.ndarray(data.shape)
    for i in range(len(data)):
        print("{} -> {}".format(i, hash(i)))
        res[hash(i)] = data[i]
    return res

if __name__ == "__main__":
    sheet = np.array([[1, 0, 0, 1],
            [0, 0, 1, 0],
            [0, 1, 0, 1],
            [1, 0, 1, 1],
            [0, 0, 1, 0]])
    hashed = minhash(sheet, hash)
    print(str(hashed))
