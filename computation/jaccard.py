import re
from nltk import ngrams

def get_jaccard_similarity(str1: str, str2: str, k: int = 1):
    a = set(ngrams(re.sub(r'[^\w\s]', '', str1).split(), k))
    b = set(ngrams(re.sub(r'[^\w\s]', '', str2).split(), k))
    inter = a.intersection(b)
    union = a.union(b)
    return (a, b, float(len(inter)) / len(union))

def main(str1, str2, k = 1):
    print(get_jaccard_similarity(str1, str2, k))

if __name__ == "__main__":
    import plac; plac.call(main)