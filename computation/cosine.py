from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np

def main():
    corpus = ["A good cook could cook as much cookies as a good cook who could cook cookies.",
        "Best chocolate chip cookies recipe.",
        "cookies",
        "how to cook chocolate cake"]
    print('# Computing cosine similarity')
    vect = TfidfVectorizer(min_df=1, stop_words='english')
    tfidf = vect.fit_transform(corpus)
    pairwise_similarity = tfidf * tfidf.T
    array = pairwise_similarity.toarray()

    print('## Display array')
    print(str(array))

    np.fill_diagonal(array, np.nan)

    i = 3
    print("## Looking for the most simular document of doc {}".format(i))
    result = np.nanargmax(array[i])
    corpus[result]
    print("Document {index}: {document}".format(index=i, document=corpus[i]))
    print("Document {index}: {document}".format(index=result, document=corpus[result]))

if __name__ == "__main__":
    import plac; plac.call(main)